<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Categories;
use App\News;

class HomeController extends Controller
{
    public function home(){
                
//        $navigation = DB::table('categories')
//            ->join('subcategories', 'categories.id', '=', 'subcategories.menu_id')
//            ->select('categories.*', 'contacts.phone', 'orders.price')
//            ->get();
        
        
        $news = News::take(3)->orderBy('news_id','desc') 
//               ->whereRaw('LENGTH(description) < 220')
               ->select('title','description')
               ->get();
        
        
        return view('index',compact('news'));
    }
}

?>