<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\News;
use App\Categories;
use App\SubCategories;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

            
    public function show($title,$id,$type)
    {  
    $webservice = '';                
        
    if($type == 1 ? $table = "categories" : $table = 'subcategories');
        
    $news = DB::table($table)        
        ->Leftjoin("webservice_category", "$table.id", '=', 'webservice_category.category')
        ->where("$table.id", $id)
        ->select('webservice_category.webservice')
        ->get();
        
        
        foreach($news as $newsWeb){
            $webservice = $newsWeb->webservice;           
        }
        if($type == 1){
        $PageName = Categories::find($id);
        }
        else{
        $PageName = SubCategories::find($id);
        }
        $MainNews = News::where(['category' => $id , 'published' => 1])
                          ->get();       
        
    if($type == "3-2"){
        $PageName = SubCategories::find($id);
        $MainNews = News::where('news_id',$id)
                    ->get();
        return view('news_view',compact('MainNews','PageName'));
    }    
    else{
        switch ($webservice){
        case '1': 
            
        return view('multiple_news',compact('MainNews','PageName'));
            
        return 'yeh 1';
        break;    
        case '2': 
        return 'yeh 2';
        break;    
        case '3': 
        return 'yeh 3';
        break;    
        case '4': 
        return 'yeh 4';
        break;    
        case '5':
        return view('news_view',compact('MainNews','PageName'));
        break; 
        case '6':
        return 'yeh 6';
        break;
        case '7':
        return 'yeh 7';
        break;
        case '8': 
        return view('member_view',compact('MainNews','PageName'));            
        return 'yeh 8';
        break;    
        case '9': 
        return 'yeh 9';
        break;    
        case '10': 
        return 'yeh 10';
        break;    
        case '11': 
        return 'yeh 11';
        break;    
        default:
        return 'somethin went wrong';   
    }    
    }
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
