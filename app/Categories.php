<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public function Subcategories(){     
      return  $this->hasMany('App\Subcategories','menu_id');
    }

    
    
public static function main(){
  $navigation = Categories::where('language','english')
      ->orderBy('order', 'asc')
      ->get();     
      return $navigation;        
    }
    
        
    
    
}
