<!DOCTYPE html>
<html>
<head>
<title>UFCW 2 - @yield('title')</title>    
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
<link type="text/css" rel="stylesheet" id="jssDefault" href="{{ asset('css/custom/theme-2.css') }}"/>
<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<link rel="stylesheet" href="{{asset('css/3calumn.css')}}">

@yield('stylesheet')    

</head>
<body>    
<body class="page-wrapper">

<!-- .preloader -->
<div class="preloader"></div>
<!-- /.preloader -->
    
<!-- main header area -->
<header class="main-header">

    <!-- header upper -->
    <div class="header-upper hidden-sm hidden-xs">
        <div class="container">
            <ul class="top-left">
                <li><i class="fa fa-envelope icon-bg" aria-hidden="true"></i>INFO@UFCW2.ORG</li>
                <li><i class="fa fa-phone icon-bg"></i>[316] 941-6290</li>
            </ul>
            <div class="top-right">
                <ul class="social-top">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                </ul>
                <div class="button-top">
                    <a href="member-profile.html" class="top-login-one style-one"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- end header upper -->

    <!-- header lower -->
    <div class="header-lower">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="logo-box">
                        <a href="index.html"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="menu-bar">
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">

                        @foreach(App\Categories::main() as $nav)
                             @if(count($nav->subcategories) >= 1)

                            <li class="dropdown"><a href="#">{{ $nav->name }}</a>
                                <ul>
                                     @foreach($nav->subcategories as $subcat)
                                        <li><a href={{ route('news', ['title' =>  strtolower(str_replace('?','',str_replace(' ','-',$subcat->name))) ,'id' => $subcat->id,'type' => 2 ]) }}>{{ $subcat->name }}</a></li>
                                     @endforeach                       
                                </ul>
                            </li>
                             @else
                                <li><a href={{ route('news', ['title' =>  str_replace('?','',strtolower(str_replace(' ','-',$nav->name))) ,'id' => $nav->id,'type' => 1 ]) }}>{{ $nav->name }}</a>
                             @endif
                        @endforeach                                    

                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end header lower -->

    <!--sticky header-->
    <div class="sticky-header">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="logo-box">
                        <a href="index.html"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="menu-bar">
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                        @foreach(App\Categories::main() as $nav)
                             @if(count($nav->subcategories) >= 1)

                            <li class="dropdown"><a href="#">{{ $nav->name }}</a>
                                <ul>
                                     @foreach($nav->subcategories as $subcat)
                                        <li><a href={{ route('news', ['title' =>  strtolower(str_replace('?','',str_replace(' ','-',$subcat->name))) ,'id' => $subcat->id,'type' => 2 ]) }}>{{ $subcat->name }}</a></li>
                                     @endforeach                       
                                </ul>
                            </li>
                             @else
                                <li><a href={{ route('news', ['title' =>  strtolower(str_replace(' ','-',$nav->name)) ,'id' => $nav->id,'type' => 1 ]) }}>{{ $nav->name }}</a>
                             @endif
                        @endforeach  
                                </ul>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end sticky header -->

</header> 
    
@yield('content')
    
    
<!-- main footer area -->
<footer class="main-footer sec-pad">
    <div class="container">
        <div class="row">

                        
            @foreach(App\Categories::main() as $nav)
                     @if(count($nav->subcategories) >= 1)
                        
            <div class="col-md-3 col-sm-6 col-xs-12 footer-colmun">
                <div class="service-widget footer-widget">
                    <div class="footer-title"><h4>{{ $nav->name }}</h4></div>
                    <ul class="list">            
                         @foreach($nav->subcategories as $subcat)
                        <li><a href={{ route('news', ['title' =>  strtolower(str_replace('?','',str_replace(' ','-',$subcat->name))) ,'id' => $subcat->id,'type' => 2 ]) }}>{{ $subcat->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>                        
                @endif
            @endforeach            
            
            
            <div class="col-md-2 col-sm-6 col-xs-12 footer-colmun">
                <div class="subscribe-widget footer-widget">
                    <div class="footer-title"><h4>Contact Us</h4></div>
                    <div class="text">
                        <p>UFCW local 2 <br/>
                            1234 Union Road,<br/>
                            Uniontown, CA 90041
                        </p>
                        <p>
                            (909) 889-8377<br/>
                            info@ufcw2.org
                        </p>
                    </div>

                    <ul class="footer-social">
                        <li><a href="#" class="active"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- main footer area end -->

<!-- footer bottom -->
<section class="footer-bottom centered">
    <div class="container">
        <div class="copyright"><p><a href="#">© 2018 UFCW Local 2.</a> All Rights Reserved. Union Website by <a href="http://www.linkedunion.com">LinkedUnion</a></p></div>

    </div>
</section>

<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-angle-up"></span></div>


<!--jquery js -->


<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/slider-active.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/wow.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/bxslider.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jQuery.style.switcher.min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>

<script src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript">
</script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.carousel.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}" type="text/javascript">
</script>
<script src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}" type="text/javascript">
</script>

<script src="{{ asset('js/script.js') }}"></script>

<!-- End of .page_wrapper -->
</body>

</html>
    
    
    
    
    