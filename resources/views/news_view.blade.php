@extends('header.top_navigation')

@section('title', $PageName->name)

@section('content')


@foreach($MainNews as $news)                            

<section class="about-section sec-pad margin-top">
    <div class="container">
        <div class="team-title centered">
            <div class="section-title"><h2>{!! $PageName->name !!}</h2></div>
            <div class="title-text">
            </div>
        </div>
        <div class="row">
            
            @if(file_exists(public_path()."/images/news/3600245.jpg"))
            <div class="col-md-3 col-sm-3 col-xs-12 about-colmun">
                <div class="about-image"><figure><img src="{{ asset('images/news/').'/'.$news->new_image }}" alt=""></figure></div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 about-colmun">
                <div class="about-content">
                    <h2 class="customizeh2">
                    
                    {{ $news->title }}
                    
                    </h2>
                    <div class="text">
                        {!! $news->description !!}
                    </div>
                </div>
            </div>
            
            @else
            
            <div class="col-md-12 col-sm-9 col-xs-12 about-colmun">
                <div class="about-content">
                    <h2 class="customizeh2">
                    
                    {{ $news->title }}
                    
                    </h2>
                    <div class="text">
                        {!! $news->description !!}
                    </div>
                </div>
            </div>
            
            
            @endif

        </div>
    </div>
</section>
@endforeach

@endsection