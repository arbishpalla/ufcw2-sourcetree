@extends('header.top_navigation')

@section('title', $PageName->name)

@section('content')

<section class="team-section sec-pad margin-top">
    <div class="container">
        <div class="team-title centered">
            <div class="section-title"><h2>{!! $PageName->name !!}</h2></div>
            <div class="title-text"><p></p>
            </div>
        </div>        
        <div class="team-page centered ">
            <div class="row row-custom justify-content-md-center">
                @foreach($MainNews as $news)                                            
                <div class="col-md-3 col-sm-6 col-xs-12 team-colmun">
                    <div class="single-item">
                        <div class="img-box">
                            <figure><img src="{{ asset('images/news/').'/'.$news->new_image }}" alt=""></figure>
                        </div>
                            <div class="lower-content">
                                <div class="team-info">
                                    <h4>{{ $news->title }}</h4>
                                    <h5><i class="fa fa-phone-square"> 316-941-6495</i></h5>
                                    <span>{!! $news->news_table !!}</span>
                                </div>
                            </div>
                    </div>
                </div>
                @endforeach                
            </div>
        </div>
    </div>
</section>


@endsection