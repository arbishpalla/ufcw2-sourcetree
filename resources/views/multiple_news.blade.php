@extends('header.top_navigation')

@section('title', $PageName->name)

@section('stylesheet')
    <style>
        span{
        position: relative !important;
        display: block !important;
        font-size: 14px !important;
        font-family: 'Open Sans',sans-serif !important;
        line-height: 25px !important;
        font-style: italic !important;
        font-weight: 400 !important;
        margin-bottom: 0px !important;    
        white-space: nowrap; 
        overflow: hidden;
        text-overflow: ellipsis;            
        }
        .team-page .single-item img {
            min-height: 341px;
        }
    </style>
@endsection

@section('content')

<section class="team-section sec-pad margin-top">
    <div class="container">
        <div class="team-title centered">
            <div class="section-title"><h2>{!! $PageName->name !!}</h2></div>
            <div class="title-text"><p></p>
            </div>
        </div>
        <div class="team-page centered">
            <div class="row row-custom justify-content-md-center">
                @foreach($MainNews as $news)                            
                <div class="col-md-4 col-sm-6 col-xs-12 team-colmun">
                    <div class="single-item">
                        <div class="img-box">
                            <figure><img src="{{ asset('images/news/').'/'.$news->new_image }}" alt=""></figure>
                        </div>
                        <a href=>
                            <div class="lower-content">
                                <div class="team-info">
                                    <h4>{!! $news->title !!}</h4>
                                    <span>{!! $news->news_table !!}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection